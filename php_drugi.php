<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="php_pierwszy_css.css"/>
        <title>PHP=1</title>
    </head>
    <?php
        if(isset($_COOKIE["kolor"]))
            $kolor = $_COOKIE["kolor"];

        if(isset($_GET["kolor"]))
        {
            $kolorZGETA = $_GET["kolor"];
            if($kolorZGETA == "zielony") $kolor="lightgreen";
            else if($kolorZGETA == "czerwony") $kolor="lightpink";
            else if($kolorZGETA == "zolty") $kolor="yellow";
            else if($kolorZGETA == "niebieska") $kolor="lightblue";
            setcookie("kolor", $kolor, mktime(23,59,59, 3,17,2018));
        }

        if(empty($kolor))
            $kolor="lightgrey";
    ?>
    <body style="background-color: <?php print($kolor);?>">
        <h3>Kolorowa strona</h3>
        <a href="?kolor=zielony"> wersja zielona </a><br/>
        <a href="?kolor=czerwony">wersja czerwona </a><br/>
        <a href="?kolor=zolty">wersja zółta </a><br/>
        <a href="?kolor=niebieska">wersja niebieska </a><br/>
    </body>
</html>