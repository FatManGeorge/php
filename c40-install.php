<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <title>Setup</title>
 <link rel="stylesheet" href="styl410.css" />
</head>
<body>
<?php
//instalacja bazy
$db = new PDO('sqlite:baza40.db');
$sql = "CREATE TABLE IF NOT EXISTS users(
 id INTEGER PRIMARY KEY, 
 user TEXT UNIQUE, 
 pass TEXT, 
 priv INT)";
$db->exec($sql);
$sql = "INSERT OR IGNORE INTO users(user,pass,priv) VALUES('admin', '".sha1('admin')."', -1)";
$db->exec($sql); 
$sql = "INSERT OR IGNORE INTO users(user,pass,priv) VALUES('jasio', '".sha1('jasio')."', 1)";
$db->exec($sql); 
$sql = "INSERT OR IGNORE INTO users(user,pass,priv) VALUES('kasia', '".sha1('kasia')."', 6)";
$db->exec($sql);
$sql = "INSERT OR IGNORE INTO users(user,pass,priv) VALUES('student', '".sha1('student')."', 14)";
$db->exec($sql); 
//uprawnienia studenta to 2 + 4 + 8 = 14
//uprawnienia kasi to 2 + 4 = 6
//uprawnienia admina to 1 + 2 + 4 + 8 + 16 + ... = -1 czyli wszystkie możliwe 
?>
 <p>Baza utworzona</p>
</body>
</html>