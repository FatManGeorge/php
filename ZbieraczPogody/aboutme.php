<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");

        PrintTitle();
        PrintNavBar();
    ?>
    <div id="Photo">
    <img src="gdansk.jpg" alt="Żuraw w gdańsku" width="400" height="300">
    </div>
    <div id="ContactForm">
        <form method="post" action="SendForm.php">
            
            <input type="text" id="fname" name="imie" placeholder="Podaj swoje imie...">

            
            <input type="text" id="fnazwisko" name="nazwisko" placeholder="Podaj swoję nazwisko...">

            
            <textarea id="fmsg" name="msg" placeholder="Podaj swoją wiadomosć" style="height:200px"></textarea>

            <input type="submit" value="Wyślij">
        </form>
    </div>
    <?php
        PrintFooter();
    ?>
</body>
</html>