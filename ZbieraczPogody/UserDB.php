<?php

    class UserDB
    {
        private $_username ="";
        private $_userpass = "";
        private $_userpriv = 0;

        public function __construct($name, $pass, $priv)
        {
            $this ->_username = $name;
            $this ->_userpass = $pass;
            $this ->_userpriv = $priv;
        }

        public function GetUserName()
        {
            return $this->_username;
        }

        public function GetPass()
        {
            return $this->_userpass;
        }

        public function GetPriv()
        {
            return $this->_userpriv;
        }
    }

    function ReturnUserDB()
    {
        $db = new PDO("sqlite:Users.db");
        $sql = "CREATE TABLE IF NOT EXISTS Uzytkownicy(
            username TEXT UNIQUE,
            passwd TEXT UNIQUE,
            priv INT)";
        $db->exec($sql);
        return $db;
    }

    function CreateFakeAdmin($db)
    {
        $sql = "INSERT OR IGNORE INTO Uzytkownicy(username, passwd, priv)
                VALUES('tomek', 'bozek', 14)";
        $db -> exec($sql);

        $sql = "INSERT OR IGNORE INTO Uzytkownicy(username, passwd, priv)
                VALUES('jasio', 'koperek', 6)";
        $db -> exec($sql);

         $sql = "INSERT OR IGNORE INTO Uzytkownicy(username, passwd, priv)
                VALUES('kasia', 'burek', 2)";
        $db -> exec($sql);
    }
?>