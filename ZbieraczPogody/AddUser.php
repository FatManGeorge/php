<?php
    include("UserDB.php");
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");

        PrintTitle();
        PrintNavBar();
        try
        {   
            CheckIfSessionExists();
            $user = $_SESSION['USER'];
            if((intval($user->GetPriv()) & 8) != 0)
            {
            ?>
                <div id="PanelForm">
                <h3>Wprowadź dane dla nowego użytkownika</h3>
                <form method="post" action="">
                    Login: <input type="text" name="login" /><br />
                    Hasło: <input type="text" name="passwd" /><br />
                    Uprawnienia: <select name="priv">
                        <option value="2">Użytkownik</option>
                        <option value="6">Użytkownik uprzywilejowany</option>
                        <option value="14">Administrator</option>
                    </select><br/>
                    <input class="submitButton" type="submit" value="Wprowadź">
                </form>
                <?php
                        AddUser(ReturnUserDB());
                ?>
                </div>
            <?php
            }
            else
            {
            ?>
                <h3>NIE Masz dostępu</h3>
            <?php
            }
        }
        catch (Exception $e)
        {
            PrintAccessDenied($e);
        }
    ?>

    <?php
        PrintFooter();

        function AddUser($db)
        {
            if(isset($_POST['login'])&&isset($_POST['passwd'])&&isset($_POST['priv']))
            {
                $login = $_POST['login'];
                $passwd = $_POST['passwd'];
                $priv = intval($_POST['priv']);

                $sql = "INSERT OR IGNORE INTO Uzytkownicy(username, passwd, priv)
                VALUES('$login', '$passwd', $priv)";
                $db->exec($sql);
                print("<h3>Poprawnie dodano użytkownika</h3>");
            }
        }
    ?>
</body>
</html>