<?php
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");
        include("UserDB.php");

        PrintTitle();
        PrintNavBar();
        if(isset($_SESSION['USER']))
        {
            header('location:Panel.php');
        }
        else
        {
            CreateFakeAdmin(ReturnUserDB());
            $sql = "SELECT * FROM Uzytkownicy";
            $resp = ReturnUserDB() -> query($sql);
            while($row = $resp->fetch())
            {
                print_r($row);
            }
        ?>
            <div id="PanelForm">
                <form method="post" action="">
                    Login: <input type="text" name="user" /><br />
                    Hasło: <input type="password" name="pass" /><br />
                    <input class="submitButton" type="submit" value="Zaloguj">
                </form>
                <h3>W celu utworzenia konta, skontaktuj się przez formularz kontaktowy</h3>
                <?php
                    if(isset($_POST['user']) && isset($_POST['pass']))
                    {
                        if(CheckIfUserExists(ReturnUserDB(), $_POST['user'], $_POST['pass']))
                        {
                            $_SESSION['USER'] = new UserDB($_POST['user'], $_POST['pass'], SetUserPriv(ReturnUserDB(),$_POST['user'], $_POST['pass']));
                            header('location:Panel.php');
                        }
                        else
                        {
                            print('<h3>NIEPOPRAWNY LOGIN LUB HASŁO</h3>');
                        }
                    }
                ?>
            </div>
        <?php
        }
    ?>
        
    <?php
        function CheckIfUserExists($db, $name, $pass)
        {
            $sql = "SELECT * FROM Uzytkownicy WHERE username = '$name' AND passwd= '$pass'";
            $resp = $db -> query($sql);
            $row = $resp -> fetch();
            if(isset($row['username']) && isset($row['passwd']) && isset($row['priv']))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        function SetUserPriv($db, $name, $pass)
        {
            $sql = "SELECT priv FROM Uzytkownicy WHERE username = '$name' AND passwd = '$pass'";
            $resp = $db -> query($sql);
            $row = $resp -> fetch();
            if(isset($row['priv']))
            {
                return intval($row['priv']);
            }
            else
            {
                return 0;
            }

        }

        PrintFooter();
    ?>
</body>
</html>