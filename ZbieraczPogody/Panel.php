<?php
    include("UserDB.php");
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");


        PrintTitle();
        PrintNavBar();
        try
        {
            CheckIfSessionExists();
            PrintAdminPanel();
        }
        catch (Exception $e)
        {
            PrintAccessDenied($e);
        }
    ?>
    <?php
        PrintFooter();

        function PrintAdminPanel()
        {
            print('<div id="PanelForm">');
            print('<ul>');
            $user = $_SESSION['USER'];
            if((intval($user->GetPriv()) & 8) != 0) PrintAddUser();
            if((intval($user->GetPriv()) & 4) != 0) PrintAddCity();
            if((intval($user->GetPriv()) & 2) != 0) PrintAddMeasurement();
            PrintLogutOut();
            print('</ul>');
            print('</div>');
        }

        function PrintAddUser()
        {
            print('<li><a href="AddUser.php">Dodaj Użytkownika</a></li>');
        }

        function PrintAddCity()
        {
            print('<li><a href="AddCity.php">Dodaj miasto</a></li>');
        }

        function PrintAddMeasurement()
        {
            print('<li><a href="AddMeasurement.php">Dodaj pomiar</a></li>');
        }

        function PrintLogutOut()
        {
            print('<li><a href="Logout.php">Wyloguj się</a></li>');
        }
    ?>
</body>
</html>