<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");

        PrintTitle();
        PrintNavBar();
    ?>
    <div id="photoGallery">
        <div class="gallery">
            <a target="_blank" href="gdansk.jpg">
            <img src="gdansk.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
        <div class="gallery">
            <a target="_blank" href="gdynia.jpg">
            <img src="gdynia.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
        <div class="gallery">
            <a target="_blank" href="gdynia.jpg">
            <img src="gdynia.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
        <div class="gallery">
            <a target="_blank" href="sopot.jpg">
            <img src="sopot.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
        <div class="gallery">
            <a target="_blank" href="warszawa.jpg">
            <img src="warszawa.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
        <div class="gallery">
            <a target="_blank" href="poznan.jpg">
            <img src="poznan.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
        <div class="gallery">
            <a target="_blank" href="krakow.jpg">
            <img src="wroclaw.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
        <div class="gallery">
            <a target="_blank" href="lublin.jpg">
            <img src="gdynia.jpg" alt="Żuraw w gdańsku" width="300" height="200">
            </a>
            <div class="desc">Gdański Żuraw nad Motławą</div>
        </div>
    </div>
    <?php
        PrintFooter();
    ?>
</body>
</html>