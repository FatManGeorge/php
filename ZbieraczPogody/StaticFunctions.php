<?php
    function AddPredefinedCities($db)
    {
        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
                VALUES('Gdańsk', 'Gdańsk')";
        $db->exec($sql);

        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
                VALUES('Gdynia', 'Gdynia')";
        $db->exec($sql);

        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
                VALUES('Sopot', 'Sopot')";
        $db->exec($sql);

        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
        VALUES('Warszawa', 'Warszawa')";
        $db->exec($sql);

        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
        VALUES('Poznań', 'Poznań')";
        $db->exec($sql);

        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
        VALUES('Kraków', 'Kraków')";
        $db->exec($sql);

        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
        VALUES('Wrocław', 'Wrocław')";
        $db->exec($sql);

        $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
        VALUES('Lublin', 'Lublin')";
        $db->exec($sql);
    }
    
    function ReturnDataBase()
    {
        $db = new PDO("sqlite:pomiary.db");
        $sql = "CREATE TABLE IF NOT EXISTS Pomiary(
            ID INTEGER PRIMARY KEY,
            miasto TEXT,
            temperatura TEXT,
            wilgotnosc INT,
            godzina_odczytu INT)";
        $db->exec($sql);
        return $db;
    }

    function GenerateFalseDataToDataBase($db, $cityDB)
    {
        $miasta = array();
        $sql = "SELECT DISTINCT miasto FROM Miasta";
        $resp = $cityDB -> query($sql);
        while($row = $resp->fetch())
        {
            array_push($miasta, $row['miasto']);
        }
        
        $sql = "SELECT count(ID) FROM Pomiary";
        $resp = $db->query($sql);
        $records = $resp->fetch();
        if($records['count(ID)']<1000)
        {
            for($i = 1; $i <= 10; $i++ )
            {
                $liczba = rand(0,count($miasta)-1);
                $r_temp = rand(-15, 40);
                $r_wilg = rand(0, 100);
                date_default_timezone_set("Europe/Warsaw");
                $czas = date("G:i:s");
                $sql = "INSERT INTO Pomiary(miasto, temperatura, wilgotnosc, godzina_odczytu) VALUES('$miasta[$liczba]','$r_temp','$r_wilg', '$czas')";
                $db->exec($sql);
            }
        }
    }

    function Return200Records()
    {
        if(isset($_GET['cityFilter']))
        {
            $cityFilter = $_GET['cityFilter'];
            $sql = SetSQLStatemant($cityFilter, 200);
        }
        else
        {
            $sql = "SELECT * FROM Pomiary ORDER BY godzina_odczytu DESC LIMIT 200";
        }

        return ReturnDataBase() -> query($sql);
    }

    function SetSQLStatemant($cityFilter, $limit)
    {
        $sql = "SELECT * FROM Pomiary WHERE miasto='$cityFilter' ORDER BY godzina_odczytu DESC LIMIT '$limit'";
        return $sql;
    }

    function FilterRecords()
    {        
        if(isset($_GET['cityFilter']))
        {
            $cityFilter = $_GET['cityFilter'];
            $sql = SetSQLStatemant($cityFilter,20);
        }
        else
        {
            $sql = "SELECT * FROM Pomiary ORDER BY godzina_odczytu DESC LIMIT 20";
        }

        return ReturnDataBase() -> query($sql);
    }

    function ReturnCityDB()
    {
        $db = new PDO("sqlite:cities.db");
        $sql = "CREATE TABLE IF NOT EXISTS Miasta(
            ID INTEGER PRIMARY KEY,
            miasto TEXT UNIQUE,
            wartosc TEXT UNIQUE)";
        $db->exec($sql);
        return $db;
    }

    function PrintAccessDenied($e)
    {
        print('<div id="AccessDenied"><h3>'.$e->getMessage().'</h3></div>');
    }

    function CheckIfSessionExists()
    {
        if(!isset($_SESSION['USER']))
        {
            throw new Exception('Nie zalogowano');
        }
    }

?>