<?php
    function PrintTitle()
    {
        print('<div id="Title">'.PHP_EOL);
        print('<h1>Zbieracz Pogody<h1>'.PHP_EOL);
        print('</div>'.PHP_EOL);
    }

    function PrintNavBar()
    {
        print('<div id="navBar">'.PHP_EOL);
        print('<ul>'.PHP_EOL);
        print('<li><a class="active" href="index.php">Strona Domowa</a></li>'.PHP_EOL);
        print('<li><a href="tabelaArch.php">Tabela</a></li>'.PHP_EOL);
        print('<li><a href="aboutme.php">O mnie</a></li>'.PHP_EOL);
        print('<li><a href="gallery.php">Galeria</a></li>'.PHP_EOL);
        print('<li id="panel"><a href="Logger.php">Panel</a></li>'.PHP_EOL);
        print('</ul>'.PHP_EOL);
        print('</div>'.PHP_EOL);
    }

    function DisplayCityFilters($db)
    {
        $sql = "SELECT * FROM Miasta";
        $filtercity = "";
        if(isset($_GET['cityFilter'])) $filtercity=$_GET['cityFilter'];
        print('<div id="filterForm">'.PHP_EOL);
        print('<form>'.PHP_EOL);
        $resp = $db->query($sql);
        while($row = $resp->fetch())
        {
            if($row['miasto'] == $filtercity)
            {
                print('         <label class="container">'.PHP_EOL);
                print('             <input class="Radio" type="radio" name="cityFilter" value="'.$row['miasto'].'" checked="checked">'.$row['wartosc'].'<br>'.PHP_EOL);
                print('             <span class="checkmark"></span>'.PHP_EOL);
                print("         </label>".PHP_EOL);
            }
            else
            {
                print('         <label class="container">'.PHP_EOL);
                print('             <input class="Radio" type="radio" name="cityFilter" value="'.$row['miasto'].'">'.$row['wartosc'].'<br>'.PHP_EOL);
                print('             <span class="checkmark"></span>'.PHP_EOL);
                print("         </label>".PHP_EOL);
            }
        }
        print('<input class="submitButton" value="Filtruj" type="submit">'.PHP_EOL);
        print('</form>'.PHP_EOL);
        print('</div>'.PHP_EOL);
    }

    function PrintFooter()
    {
        print('<div id="footer">'.PHP_EOL);
        print(' <h1>WSZ INFORMATYKA 411</h1>'.PHP_EOL);
        print(' <h1>Programowanie Aplikacji internetowych</h1>'.PHP_EOL);
        print(' <h1>Chlechowicz Grzegorz</h1>'.PHP_EOL);
        print('</div>'.PHP_EOL );
    }
?>