<?php
    include("UserDB.php");
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");

        PrintTitle();
        PrintNavBar();
        try
        {   
            CheckIfSessionExists();
            $user = $_SESSION['USER'];
            if((intval($user->GetPriv()) & 2) != 0)
            {
            ?>
                <div id="PanelForm">
                    <h3>Wprowadź dane dla nowego pomiaru</h3>
                    <form method="post" action="">
                        Miasto: <select name="miasto">
                            <?php
                                PrintCityOptions(ReturnCityDB());
                            ?>
                        </select><br/>
                        Temperatura: <input type="text" name="temp" /><br />
                        Wilgotność: <input type="text" name="wilg" /><br />
                        <input class="submitButton" type="submit" value="Wprowadź">
                    </form>
                    <?php
                            AddMeasurement(ReturnDataBase());
                    ?>
                </div>
            <?php
            }
            else
            {
            ?>
                <h3>NIE Masz dostępu</h3>
            <?php
            }
        }
        catch (Exception $e)
        {
            PrintAccessDenied($e);
        }
    ?>

    <?php
        PrintFooter();
        
        function AddMeasurement($db)
        {
            if(isset($_POST['miasto']) && isset($_POST['temp']) && isset($_POST['wilg']))
            {
                $miasto = $_POST['miasto'];
                $temp = $_POST['temp'];
                $wilg = $_POST['wilg'];
                date_default_timezone_set("Europe/Warsaw");
                $czas = date("G:i:s");
                $sql = "INSERT INTO Pomiary(miasto, temperatura, wilgotnosc, godzina_odczytu) VALUES('$miasto','$temp','$wilg', '$czas')";
                $db->exec($sql);
                print('<h3>Pomiar został poprawnie wprowadzony</h3>');
            }
        }

        function PrintCityOptions($db)
        {
            $sql = "SELECT DISTINCT miasto FROM Miasta";
            $resp = $db->query($sql);
            while($row = $resp->fetch())
            {
                print('<option value="'.$row['miasto'].'">'.$row['miasto'].'</option>'.PHP_EOL);
            }
        }
    ?>
</body>
</html>