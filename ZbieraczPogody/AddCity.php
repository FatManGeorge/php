<?php
    include("UserDB.php");
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");

        PrintTitle();
        PrintNavBar();
        try
        {   
            CheckIfSessionExists();
            $user = $_SESSION['USER'];
            if((intval($user->GetPriv()) & 4) != 0)
            {
            ?>
                <div id="PanelForm">
                <h3>Wprowadź dane dla nowego pomiaru</h3>
                <form method="post" action="">
                    Dodaj miasto: <input type="text" name="miasto" /><br />
                    <input class="submitButton" type="submit" value="Wprowadź">
                </form>
                <?php
                        AddCity(ReturnCityDB());
                ?>
                </div>
            <?php
            }
            else
            {
            ?>
                <h3>NIE Masz dostępu</h3>
            <?php
            }
        }
        catch (Exception $e)
        {
            PrintAccessDenied($e);
        }
    ?>

    <?php
        PrintFooter();

        function AddCity($db)
        {
            if(isset($_POST['miasto']))
            {
                $miasto = substr(strip_tags($_POST['miasto']),0,50);
                $sql = "INSERT OR IGNORE INTO Miasta(miasto, wartosc)
                        VALUES('$miasto', '$miasto')";
                $db->exec($sql);
                print("<h3>Poprawnie dodano nowe miasto</h3>");
            }
        }
    ?>
</body>
</html>