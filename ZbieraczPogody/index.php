<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="MyStyle.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zbieracz Pogody</title>
</head>
<body>
    <?php
        include("StaticFunctions.php");
        include("StaticElements.php");
        AddPredefinedCities(ReturnCityDB());
        
        PrintTitle();
        PrintNavBar();
        DisplayCityFilters(ReturnCityDB());
        
    ?>
    <table id="wheaterTable">
        <tr>
            <th>L.P.</th>
            <th>Miasto</th>
            <th>Temperatura</th>
            <th>Wilgotność</th>
            <th>Godzina pomiarów</th>
        </tr>
        <?php
            GenerateFalseDataToDataBase(ReturnDataBase(), ReturnCityDB());
            
            $resp = FilterRecords(); 

            $id = 1;
            while($row = $resp->fetch())
           {
                print'<tr>';
                print'<td>'.$id.'</td>'.PHP_EOL;
                print'<td>'.$row['miasto'].'</td>'.PHP_EOL;
                print'<td>'.$row['temperatura'].'</td>'.PHP_EOL;
                print'<td>'.$row['wilgotnosc'].'</td>'.PHP_EOL;
                print'<td>'.$row['godzina_odczytu'].'</td>'.PHP_EOL;
                print'</tr>'.PHP_EOL;
                $id += 1;
            }  
        ?>
    </table>
    <?php
        PrintFooter();
    ?>
</body>
</html>