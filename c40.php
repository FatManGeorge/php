<?php
  session_start();
?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8" />
 <title>Uwierzytelnianie</title>
 <link rel="stylesheet" href="styl410.css" />
</head>
<body>
 <h3>Autoryzacja</h3>
 <form method="post" action="">
 Login: <input type="text" name="user" /><br />
 Hasło: <input type="password" name="pass" /><br />
 <input type="submit" />
 </form>
 <hr />
<?php
  if(isset($_POST['user'])) $user = substr(strip_tags($_POST['user']),0,40);
  if(isset($_POST['pass'])) $pass = substr(strip_tags($_POST['pass']),0,40);
  if(!empty($user) && !empty($pass))
    {
    $db = new PDO('sqlite:baza40.db');
    $sql = "SELECT priv FROM users WHERE user=:usr AND pass=:prv";
    $res = $db->prepare($sql);
    $res->bindValue(':usr', $user);
    $res->bindValue(':prv', sha1($pass));
    $res->execute();
    $r = $res->fetch();
    if(isset($r['priv'])) $_SESSION['upr'] = intval($r['priv']); 
    else $_SESSION['upr'] = 0;
    $_SESSION['login'] = $user;
    }
 //linki
 if(isset($user)) print("<p>Witaj $user ({$_SESSION['upr']})</p>");
 if(isset($_SESSION['upr'])) $upr = $_SESSION['upr']; else $upr = 0;
 if(($upr & 1) != 0) print('<a href="c41.php">Jeden</a><br />');     
 if(($upr & 2) != 0) print('<a href="c42.php">Dwa</a><br />');     
 if(($upr & 4) != 0) print('<a href="c43.php">Trzy</a><br />');     
 if(($upr & 8) != 0) print('<a href="c44.php">Cztery</a><br />');     
?>
</body>
</html>