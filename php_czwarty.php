<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="gch.css"/>
        <title>PHP - POD</title>
    </head>
    <body>
        <?php
            $db = new PDO("sqlite:bazadanych_cwiczenia.db");
            $sql = "CREATE TABLE IF NOT EXISTS wycieczki(
                id INTEGER PRIMARY KEY,
                nazwa TEXT UNIQUE,
                miejsce TEXT,
                czas INT,
                cena INT)";
            $db->exec($sql);
            
            if(isset($_POST['nazwa']) && isset($_POST['miejsce']) && isset($_POST['czas']) && isset($_POST['cena']))
            {
                $nazwa = substr(strip_tags($_POST['nazwa']),0,60);
                $miejsce = substr(strip_tags($_POST['miejsce']),0,60);
                $czas = intval($_POST['czas']);
                $cena = intval($_POST['cena']);

                $sql = "INSERT INTO wycieczki(nazwa, miejsce, czas, cena) VALUES(:naz, :mie, :cza, :cena)";
                $res = $db->prepare($sql);
                $res->bindValue(':naz', $nazwa);
                $res->bindValue(':mie', $miejsce);
                $res->bindValue(':cza', $czas);
                $res->bindValue(':cena', $cena);
                $k = $res->execute();

                if($k>0)
                    print('<p class="success">Dane poprawne</p>');
                else
                    print('<p class="error">niepoprawne</p>');
            }
        ?>
        <h3>Wycieczki</h3>
        <form method="post" action="">
            Nazwa: <input type="text" name="nazwa" id="fnazwa" required="required" placeholder="Nazwa imprezy" /></br>
            Lokalizacja : <input type="text" name="miejsce" id="fmiejsce" required="required" placeholder="Miejsce wycieczki" /></br>
            Czas trwania : <input type="number" name="czas" id="fczas" min="3" max="15" required="required" /> </br>
            Cena : <input type="number" name="cena" id="flokalizacja" required="required" pattern="^[0-9]+$" /></br>
            <input type="submit" value="Zapisz" />
        </form>
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th>Miejsce</th>
                    <th>Czas</th>
                    <th>Cena</th>
                </tr>
            </thead>
            <?php
            $sql = "SELECT * FROM wycieczki ORDER BY nazwa LIMIT 1000";
            $res = $db-> query($sql);
            while($row = $res->fetch())
            {
                print'<tr>';
                print'<td>'.$row['id'].'</td>';
                print'<td>'.$row['nazwa'].'</td>';
                print'<td>'.$row['miejsce'].'</td>';
                print'<td>'.$row['czas'].'</td>';
                print'<td>'.$row['cena'].'</td>';
                print'</tr>'.PHP_EOL;
            }
        ?>
        </table>
    </body>
</html>
