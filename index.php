<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="mainpage.css"/>
        <title>Sprawdź Pogodę w Polsce</title>
    </head>
    <body>
        <div class="HeaderTitle">
            <h1>3City<br/>Wheater</h1>
        </div>
        <div id="navBar">
            <ul>
                <li><a class="active" href="#home">Home</a></li>
                <li><a href="#news">News</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="#about">About</a></li>
            </ul>
        </div>
        <div class="vertical-menu">
            <a href="#" class="active">Home</a>
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
            <a href="#">Link 4</a>
        </div>
        <table id="viewTable">
            <tr>
                <th>L.P.</th>
                <th>Miasto</th>
                <th>Temperatura</th>
                <th>Wilgotność</th>
                <th>Godzina pomiarów</th>
            </tr>
            <?php
                include("DataBaseGenerator.php");
                $db = new PDO("sqlite:bazadanych_cwiczenia.db");
                $sql = "CREATE TABLE IF NOT EXISTS odczyty(
                    id INTEGER PRIMARY KEY,
                    miasto TEXT,
                    temperatura TEXT,
                    wilgotnosc INT,
                    godzina_odczytu INT)";
                $db->exec($sql);
                
                $res = $db->query("SELECT count(id) FROM odczyty");
                $row = $res->fetch();
                
                if($row['count(id)'] < 200)
                    GenerateFalseDate($db);
                #GenerateFalseDate($db);
                #PredefinedFalseData();
                $sql = "SELECT * FROM odczyty ORDER BY id";
                $res = $db-> query($sql);
                
                while($row = $res->fetch())
                {
                    print'<tr>';
                    print'<td>'.$row['id'].'</td>';
                    print'<td>'.$row['miasto'].'</td>';
                    print'<td>'.$row['temperatura'].'</td>';
                    print'<td>'.$row['wilgotnosc'].'</td>';
                    print'<td>'.$row['godzina_odczytu'].'</td>';
                    print'</tr>';
                }
            ?>
        </table>
    </body>
</html>